﻿Public Class Trakt
    Structure tokens
        Dim AccessToken As String
        Dim RefreshToken As String
        Dim Expire As DateTime
    End Structure
    Shared Function GetAcessToken(ByVal code As String, ByVal client_id As String, ByVal client_secret As String) As tokens
        Dim token As New tokens
        Dim request = TryCast(System.Net.WebRequest.Create("https://api-v2launch.trakt.tv/oauth/token"), System.Net.HttpWebRequest)
        request.Method = "POST"
        request.ContentType = "application/json"

        Using writer = New System.IO.StreamWriter(request.GetRequestStream())
            Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes("{\""code\"": \""" & code & "\"",\""client_id\"": \""" & client_id & "\"",\""client_secret\"": \""" & client_secret & "\"",\""redirect_uri\"": \""urn:ietf:wg:oauth:2.0:oob\"",\""grant_type\"": \""authorization_code\""}")
            request.ContentLength = byteArray.Length
            writer.Write(byteArray)
            writer.Close()
        End Using
        Dim responseContent As String
        Using response = TryCast(request.GetResponse(), System.Net.HttpWebResponse)
            Using reader = New System.IO.StreamReader(response.GetResponseStream())
                responseContent = reader.ReadToEnd()
            End Using
        End Using
        Debug.WriteLine(responseContent)
        Return token
    End Function
End Class
